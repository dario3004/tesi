/*
 * VisualOdometry.h
 *
 *  Created on: May 25, 2014
 *      Author: dario
 */

#ifndef VISUALODOMETRY_H_
#define VISUALODOMETRY_H_

#include <Eigen/Dense>	//used for motion threshold matrix
using Eigen::MatrixXd;

class VisualOdometry {

private:
	MatrixXd odometry;

	class Tracker{

	};

	class Mapper{

	};

public:

	/**
	 * @return current odometry
	 */
	MatrixXd getOdometry(){
		return odometry;
	}

	/**
	 * Sets current odometry
	 * @param odm
	 */
	void setOdometry(MatrixXd& odm){
		this->odometry = odm;
	}

	VisualOdometry(){

	};

	~VisualOdometry(){
		this->odometry.resize(0,0);
	};
};

#endif /* VISUALODOMETRY_H_ */
