//============================================================================
// Name        : Main.cpp
// Author      : Dario
// Version     :
// Copyright   : your copy notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <Eigen/Dense>	//used for covariance matrix
#include <Eigen/Core>
#include <iostream>
#include <vector>
#include "particle/MotionModel.h"
#include "particle/Particle.h"
#include "LayoutManager.h"
#include "VisualOdometry.h"
#include "detectors/LowLevelDetector.h"

using namespace Eigen;
using namespace std;

int main() {

	cout << "------------ INIZIO ESECUZIONE MAIN() ------------" << endl;

	// ---------- istanzio motion model ---------------------------------------------------- //
	MotionModel mtn_model; // Error covariance: 12x12 [0]
						   // Delta_t: 33.33 milliseconds
	//mtn_model.toString();
	cout << "Creazione motion model OK" << endl;

	// ---------- costruisco 3 particelle -------------------------------------------------- //

	// pose vettore 12x1
	VectorXd p_pose1 = VectorXd::Zero(12);
	VectorXd p_pose2 = VectorXd::Zero(12);
	VectorXd p_pose3 = VectorXd::Zero(12);

	Particle p1(1, p_pose1, mtn_model);
	Particle p2(2, p_pose2, mtn_model);
	Particle p3(3, p_pose3, mtn_model);
	cout << "Creazione particle OK" << endl;

	// ---------- aggiungo delle componenti alle particelle -------------------------------- //
	// (le componenti possono essere solo dello stesso tipo dei detector presenti)
	VectorXd comp_pose1 = VectorXd::Zero(12);
	VectorXd comp_pose2 = VectorXd::Zero(12);

	ParticleComponent building1(comp_pose1);
	ParticleComponent building2(comp_pose2);
	cout << "Creazione ParticleComponent OK" << endl;


	// ---------- aggiungo le componenti alle particelle ----------------------------------- //
	ParticleComponent* particle_pointer = &building1;
	p1.addComponent(particle_pointer);
	p2.addComponent(particle_pointer);
	p3.addComponent(particle_pointer);

	particle_pointer = &building2;
	p1.addComponent(particle_pointer);
	p2.addComponent(particle_pointer);
	p3.addComponent(particle_pointer);
	cout << "Aggiunta componenti alle particelle OK" << endl;

	// ---------- creo il particle set ----------------------------------------------------- //
	vector<Particle> particle_set;
	particle_set.push_back(p1);
	particle_set.push_back(p2);
	particle_set.push_back(p3);
	cout << "Pushback delle particelle nel particle_set OK" << endl;

	// ---------- inserisco il particle-set per la prima volta nel layout-manager ---------- //
	VisualOdometry visual_odometry;
	LayoutManager layout_manager(particle_set, visual_odometry);
	cout << "Creazione visual_odometry e layout_manager OK" << endl;

	// ---------- istanzio low level detectors --------------------------------------------- //
	LowLevelDetector low_level_detector1;
	LowLevelDetector low_level_detector2;
	cout << "Creazione lowleveldetector OK" << endl;

	// ---------- imposto odometry --------------------------------------------------------- //
	MatrixXd odm = MatrixXd::Ones(12,12);
	layout_manager.setOdometry(odm);
	// ------------------------------------------------------------------------------------- //

	//simulo il detection di una nuova Low Level Feature richiamando Layout Estimation
	//layout_manager.layoutEstimation();


//	VectorXd vect(12);
//	vect << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12;
//	cout << "vect: " << endl << vect << endl;
//
//	Particle prova(5, mtn_model, vect);
//	cout << "Particle state: " << endl << prova.getParticleState() << endl;
//	cout << "Particle pose t+1: " << endl << prova.propagateParticlePose() << endl;
//
//	MatrixXd m = MatrixXd::Identity(12, 12);
//	cout << "identity" << endl << m << endl;
//
//	MatrixXd G_t = mtn_model.motionJacobi(vect);
//	MatrixXd H_t = mtn_model.measurementJacobi(vect);
//
//	cout << "G_t" << endl << G_t << endl;
//	cout << "H_t" << endl << H_t << endl;


// -------------------------------------------------------------------------
	VectorXd stato_t(12);	/// state
	MatrixXd E_t = MatrixXd::Ones(12,12);	/// sigma (state error covariance)

	VectorXd stato_t_predetto = VectorXd::Zero(12);	/// predicted state
	MatrixXd E_t_pred = MatrixXd::Zero(12,12);	/// predicted sigma (state error covariance)
	MatrixXd R_t = MatrixXd::Ones(12,12);	/// motion error covariance
	MatrixXd Q_t = MatrixXd::Ones(12,12);	/// measure error covariance

	MatrixXd G_t = MatrixXd::Zero(12,12);	/// motion equations jacobian
	MatrixXd H_t = MatrixXd::Zero(12,12);	/// measure equations jacobian
	MatrixXd K_t = MatrixXd::Zero(12,12);	/// Kalman gain

	VectorXd p_pose = VectorXd::Zero(12);
	for(int i=6; i<12; i++)
		p_pose[i] = 1;

	Particle particle(99, p_pose, mtn_model);
// -------------------------------------------------------------------------
	stato_t_predetto = particle.propagateParticlePose();

	cout << "Particle state: " << endl << particle.getParticleState() << endl;
	cout << "Stato_t_predetto: " << endl << stato_t_predetto << endl;

	G_t = particle.mtn_model.motionJacobi(stato_t_predetto);
	E_t_pred = G_t * E_t * G_t.transpose() + R_t;
	cout << "G_t: " << endl << G_t << endl;
	cout << "E_t_pred: " << endl << E_t_pred << endl;

	H_t = particle.mtn_model.measurementJacobi(stato_t_predetto);

	//MatrixXd temp = H_t * E_t_pred * H_t.transpose() + Q_t;
	MatrixXd temp = MatrixXd::Identity(12, 12);

	K_t = E_t_pred * H_t.transpose() * temp.inverse();
	cout << "H_t" << endl << H_t << endl;
	cout << "Temp" << endl << temp << endl;
	cout << "K_t: " << endl << K_t << endl;

//  % calcolo belief:
	stato_t = stato_t_predetto;// + K_t;
	E_t = (MatrixXd::Identity(12,12) - K_t * H_t) * E_t_pred;

	cout << "stato_t" << endl << stato_t << endl;
	cout << "E_t: " << endl << E_t << endl;


	cout << "------------ FINE ESECUZIONE MAIN() ------------" << endl;
	return 0;
}


//int main(){
//	MatrixXd a(3,3);
//	a << 10, 10, 10,
//	     4, 5, 6,
//	     7, 8, 9;
//
//
//	MatrixXd b(3,3);
//	b << 1, 2, 3,
//	     4, 5, 6,
//	     7, 8, 9;
//
//	MatrixXd c(3,3);
//	c = (a.array() > b.array()).cast<double>();
//	cout << c.count() << endl;
//
//	return 0;
//}
