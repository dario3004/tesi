/*
 * HighLevelDetector.h
 *
 *  Created on: May 25, 2014
 *      Author: dario
 */

#ifndef HIGHLEVELDETECTOR_H_
#define HIGHLEVELDETECTOR_H_


#include "../LayoutManager.h" 	  // Used for taking feedback from the scene layout
#include "LowLevelDetector.h" // Used for data acquisition from low-level detectors

class HighLevelDetector {

protected:
//	template<typename CANDIDATES>
//	CANDIDATES detector_results;	/// HighLevelDetectors can view each other results

private:

	/**
	 * Get results of (low-level) detector passed as parameter
	 * @param detector
	 */
	template<typename DETECTOR>
	void getLowLevelResults(DETECTOR detector);

	/**
	 * Detect candidates given the current layout and other detectors datas (if needed)+-
	 * @param p_set
	 * @param other_detectors_results
	 */
	template<typename PARTICLE_SET, typename OTHER>
	void findCandidates(PARTICLE_SET p_set, OTHER other_detectors_results);

public:

	/**
	 * Returns detector results (used by LayoutManager etc.)
	 * @return
	 */
	template<typename CANDIDATES>
	CANDIDATES getDetectorResults();

	// constructor e destructor
	HighLevelDetector();
	virtual ~HighLevelDetector();
};

#endif /* HIGHLEVELDETECTOR_H_ */
