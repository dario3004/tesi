/*
 * LowLevelDetectorManager.h
 *
 *  Created on: May 30, 2014
 *      Author: dario
 */

#ifndef LOWLEVELDETECTORMANAGER_H_
#define LOWLEVELDETECTORMANAGER_H_

#include "LowLevelDetector.h"
#include "../FeedbackCollector.h"

class LowLevelDetectorManager {
private:
	LowLevelDetector detector_camera1;
	LowLevelDetector detector_camera2;
	FeedbackCollector feedback_collector;

public:
	LowLevelDetectorManager();
	virtual ~LowLevelDetectorManager();
};

#endif /* LOWLEVELDETECTORMANAGER_H_ */
