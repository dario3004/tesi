/*
 * Detector3D.h
 *
 *  Created on: May 25, 2014
 *      Author: dario
 */

#ifndef DETECTOR3D_H_
#define DETECTOR3D_H_

class Detector3D {
private:
	class Matcher{

	};

	class FeatureDetector{

	};

	class LineDetector{

	};

	class RegionDetector{

	};

public:

	/**
	 * Returns detector results
	 * @return
	 */
	template<typename DETECTOR3D_RESULTS>
	DETECTOR3D_RESULTS getDetector3DResults();

	// constructor, deconstructor
	Detector3D();
	virtual ~Detector3D();
};

#endif /* DETECTOR3D_H_ */
