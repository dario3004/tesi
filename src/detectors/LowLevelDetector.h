/*
 * LowLevelDetector.h
 *
 *  Created on: May 25, 2014
 *      Author: dario
 */

#ifndef LOWLEVELDETECTOR_H_
#define LOWLEVELDETECTOR_H_

class LowLevelDetector {

private:
	class FeatureDetector{

	};

	class LineDetector{

	};

	class RegionDetector{

	};
public:
	LowLevelDetector();
	virtual ~LowLevelDetector();
};

#endif /* LOWLEVELDETECTOR_H_ */
