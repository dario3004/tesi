/*
 * FeedbackCollector.h
 *
 *  Created on: May 30, 2014
 *      Author: dario
 */

#ifndef FEEDBACKCOLLECTOR_H_
#define FEEDBACKCOLLECTOR_H_

class FeedbackCollector {
public:
	FeedbackCollector();
	virtual ~FeedbackCollector();
};

#endif /* FEEDBACKCOLLECTOR_H_ */
