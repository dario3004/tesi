/*
 * MotionModel.h
 *
 *  Created on: Jun 2, 2014
 *      Author: dario
 */

#ifndef MOTIONMODEL_H_
#define MOTIONMODEL_H_

#include "ParticleComponent.h"
#include <vector>		//used for pose vector
#include <Eigen/Dense>	//used for covariance matrix
#include <Eigen/Core>	//used for covariance matrix
#include <iostream>

using namespace Eigen;
using namespace std;
using Eigen::MatrixXd;
using std::vector;

/**
 * This class is used by every particle in order to propagate itself using the
 * defined motion-model.
 * In case there aren't enough datas from Odometry sensors, a decadency motion model
 * will be applied to propagate the particle
 */
class MotionModel {
private:
	MatrixXd error_covariance; 	/// 12x12 (double) error covariance matrix
								/// 6DoF + 6 derivate(velocita su ogni DoF)

	double delta_t;	/// Time interval between t and t+1

	/**
	 * In case there aren't enough datas from Odometry sensors, a decadency motion model
	 * will be applied to propagate the particle
	 */
	void decadencyMotionModel();

public:

	/**
	 * This function will propagate the particle component using the defined
	 * motion-model inside MotionModel.cpp
	 * @param p_component
	 */
	void propagateComponent(ParticleComponent* p_component);

	VectorXd propagatePose(VectorXd& particle_state);
	VectorXd propagatePose(VectorXd& particle_state, VectorXd& control);


	MatrixXd motionJacobi(VectorXd& p_state_predicted);
	MatrixXd measurementJacobi(VectorXd& p_state_predicted);

	void toString(){
		cout << "Motion model, delta_t = " << this->delta_t << " error covariance = " << endl;
		cout << this->error_covariance << endl;
	}

	/**
	 * Istantiate MotionModel with the given error covariance, and delta_t to 30fps if it's not passed as argument
	 * @param cov
	 * @param dt
	 */
	MotionModel(const MatrixXd& cov) : error_covariance(cov), delta_t(33.33){};
	MotionModel(const MatrixXd& cov, const double dt) : error_covariance(cov), delta_t(dt){};

	/**
	 * Default constructor
	 */
	MotionModel() {
		// Sets delta_t as 30fps -> 33.33 milliseconds
		this->delta_t = 33.33;

		// Sets the error covariance to zero
		MatrixXd cov(12,12);
		cov.Zero(12,12);
		this->error_covariance = cov;
	};

	//destructor
	virtual ~MotionModel(){
		error_covariance.resize(0,0);
	};
};

#endif /* MOTIONMODEL_H_ */
