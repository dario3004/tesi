/*
 * Particle.h
 *
 *  Created on: Jun 2, 2014
 *      Author: dario
 */

#ifndef PARTICLE_H_
#define PARTICLE_H_

#include "ParticleComponent.h"
#include "MotionModel.h"
#include <vector>
#include <Eigen/Dense>	//used for pose matrix
#include <Eigen/Core>
using namespace Eigen;
using std::vector;

class Particle {

private:
	unsigned int id;		/// particle id
	VectorXd particle_state;	/// particle state (12x12: 6DoF pose + 6 Speed Derivates)
	vector<ParticleComponent*> particle_components; /// array of particle-components

public:
	MotionModel mtn_model;	/// particle motion model

	/**
	 * This function will use "motion-model" class to propagate all particle's components
	 */
	void propagateParticleComponents();

	/**
	 * This function will use "motion-model" class to propagate particle pose
	 */
	VectorXd propagateParticlePose();
	MatrixXd motionJacobi(VectorXd& p_state_predicted);
	MatrixXd measurementJacobi(VectorXd& p_state_predicted);

	/**
	 * This function will calculate all the components' score
	 */
	void calculateComponentsScore();


	//getters&setters -------------------------------------------------------------
	int getId() const {
		return id;
	}

	void setId(int id) {
		this->id = id;
	}

	vector<ParticleComponent*> getParticleComponents(){
		return particle_components;
	}

	VectorXd getParticleState(){
		return particle_state;
	}

	void setParticleState(const VectorXd& p_state){
		particle_state = p_state;
	}


	/**
	 * Add new component to the particle
	 * @param component
	 */
	void addComponent(ParticleComponent* component){
		this->particle_components.push_back(component);
	}

	/**
	 * Remove the selected component from the particle
	 * @param component
	 */
	void removeComponent(ParticleComponent* component){
//		this->particleComponents.erase(component);
//
//
//		#include <algorithm>
//		std::vector<int>::iterator position = std::find(vector.begin(), vector.end(), 8);
//		if (position != vector.end()) // == vector.end() means the element was not found
//			myVector.erase(position);
	}

	//constructor -------------------------------------------------------------
	Particle();
	Particle(unsigned int num, MotionModel& mt_md) : id(num), mtn_model(mt_md) {};
	Particle(unsigned int num, VectorXd& pose, MotionModel& mt_md )
		: id(num), particle_state(pose), mtn_model(mt_md)  {};


	//destructor
	virtual ~Particle() {
//		id=0;
//		vector<ParticleComponent*>::iterator itr;
//		for (itr = particleComponents.begin(); itr != particleComponents.end(); itr++){
//			ParticleComponent* comp = *itr;
//			delete comp;
//		}
	}

	//copy constructor

	//assignment


};

#endif /* PARTICLE_H_ */
