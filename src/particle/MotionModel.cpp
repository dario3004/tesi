/*
 * MotionModel.cpp
 *
 *  Created on: Jun 2, 2014
 *      Author: dario
 */

#include "MotionModel.h"
#include "ParticleComponent.h"
#include <Eigen/Core>
#include <Eigen/Dense>
#include <iostream>
using namespace std;
using namespace Eigen;

void MotionModel::propagateComponent(ParticleComponent* p_component){
	//motion-model
	cout << "Propagating component" << endl;
}

VectorXd MotionModel::propagatePose(VectorXd& p_state){
	// X_t ----> X_t+1
	//
	// s_t+1 = s_t + v_t * Delta_t + R

	VectorXd p_pose(6);
	VectorXd p_vel(6);

	p_pose << p_state[0], p_state[1], p_state[2], p_state[3], p_state[4], p_state[5];
	p_vel << p_state[6], p_state[7], p_state[8], p_state[9], p_state[10], p_state[11];

	VectorXd error_pose(6);
	error_pose << error_covariance(0,0), error_covariance(1,1), error_covariance(2,2), error_covariance(3,3), error_covariance(4,4), error_covariance(5,5);

	p_pose = p_pose + (p_vel * delta_t) + error_pose;

	return p_pose;
}


MatrixXd MotionModel::motionJacobi(VectorXd& p_state_predicted){
	/**
	 * G_t:
	 *
	 *  | 1 0 0 0 0 0  Dt 0 0 0 0 0 |
	 *  | 0 1 0 0 0 0  0 Dt 0 0 0 0 |
	 *  | 0 0 1 0 0 0  0 0 Dt 0 0 0 |
	 *  | 0 0 0 1 0 0  0 0 0 Dt 0 0 |
	 *  | 0 0 0 0 1 0  0 0 0 0 Dt 0 |
	 *  | 0 0 0 0 0 1  0 0 0 0 0 Dt |
	 *
	 * 	| 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 */


	// Create a zero 12x12 matrix
	MatrixXd G_t = MatrixXd::Zero(12,12);

	// Sets diagonal of first 6x6 matrix to 1
	for(int i = 0; i<6; ++i)
		G_t(i,i) = 1;

	// Sets diagonal of first 6x6 matrix to delta_t
	for(int i = 0; i<6; i++)
		G_t(i,i+6) = delta_t;

	return G_t;
}


MatrixXd MotionModel::measurementJacobi(VectorXd& p_state_predicted){
	/**
	 * H_t:
	 *
	 *  | 1 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 1 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 1 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 1 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 1 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 1  0 0 0 0 0 0 |
	 *
	 * 	| 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 *  | 0 0 0 0 0 0  0 0 0 0 0 0 |
	 */

	// Create a zero 12x12 matrix
	MatrixXd H_t = MatrixXd::Zero(12,12);

	// Sets diagonal of first 6x6 matrix to 1
	for(int i = 0; i<6; ++i)
		H_t(i,i) = 1;

	return H_t;
}
